#include"game.h"

void InitBoard(char board[ROW][COL], int row, int col)
{
	int i = 0;
	for ( i = 0; i < row; i++)
	{
		int j = 0;
		for ( j = 0; j < col; j++)
		{
			board[i][j] = ' '; 
		}
	}
}


void DisplayBoard(char board[ROW][COL], int row, int col)
{

	int i = 0;
	for ( i = 0; i < row; i++)
	{
		//打印数据
	
		int j = 0;
		for ( j = 0; j < col; j++)
		{
			printf(" %c ", board[i][j]);
			if (j<col-1)
				printf("|");
		}
		printf("\n");
		if (i < row - 1)
		{
			for (j = 0; j < col; j++)
			{
				printf("---");
				if (j < col - 1)
					printf("|");
			}
		}
		printf("\n");
	}
}

void PlayMove(char board[ROW][COL], int row, int col)
{
	printf("玩家走：请输入坐标>\n");
	int x = 0;
	int y = 0;
	while (1)
	{
		scanf_s("%d%d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (board[x - 1][y - 1]==' ')
			{
				board[x - 1][y - 1] ='*';
				break;
			}
			else
			{
				printf("坐标被占用,重新输入:>");
			}
		}
		else
		{
			printf("输入错误，请重新输入\n");
		}
	}
}

void ComputerMove(char board[ROW][COL], int row, int col)
{
	int x = 0;
	int y = 0;
	printf("电脑走>:\n");
	while (1)
	{

		x = rand() % row;
		y = rand() % col;
		if (board[x][y] == ' ')
		{
			board[x][y] = '#';
			break;
		}
	}
}

char IsWin(char board[ROW][COL], int row, int col)
{
	int i = 0;                                              //1、判断输赢
	for (i = 0; i < row; i++)                                                               //三行相同获胜
	{
		if (board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] != ' ')
		{
			return board[i][0];
		}
	}
	for (i = 0; i < col; i++)                                                               //三列相同获胜
	{
		if (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] != ' ')
		{
			return board[0][i];
		}
	}
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ')//对角线相同获胜
	{
		return board[0][0];
	}
	if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
	{
		return board[1][1];
	}
	for (i = 0; i < row; i++)                          //2、如果格子还没下满，则返回C继续
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
			{
				return 'C';
			}
		}
	}
	return 'Q';                                      //3，如果格子满了，说明平局返回Q
}