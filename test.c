#include "game.h"
void menu()
{
	printf("*********************\n");
	printf("****** 1.play *******\n");
	printf("****** 0.exit *******\n"); 
	printf("*********************\n");
}

void game()
{
	char board[ROW][COL];
	InitBoard(board, ROW, COL);//棋盘的初始化
	DisplayBoard(board, ROW, COL);//打印棋盘
	char ret = 0;
	while (1)                              //进入下棋过程
	{
		PlayMove(board, ROW, COL);//玩家下
		DisplayBoard(board, ROW, COL);//打印棋盘
		ret = IsWin(board, ROW, COL);//判断输赢
		if (ret!='C')
		{
			break;
		}
		ComputerMove(board, ROW, COL);//电脑下
		DisplayBoard(board, ROW, COL);//打印棋盘
		ret = IsWin(board, ROW, COL);//判断输赢
		if (ret != 'C')
		{
			break;
		}
	}
	if (ret == '*')
	{
		printf("玩家赢\n");

	}
	else if (ret == '#')
	{
		printf("电脑赢\n");
	}
	else 
	{
		printf("平局\n");
	}
}
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();//打印游戏菜单
		printf("请选择>\n");
		scanf_s("%d", &input); //输入玩家的选择
		switch (input)//根据玩家的选择，进入到不同的分支，其中1进入我们的游戏函数，0退出游戏，如果有其他输入的话提示选择错误请重新输入
		{
		case 1:
			printf("三子棋\n");
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误\n");
			break;

		}
	} while (input);
	return 0;
}